# README #

### Application for playing hls live stream in different platform without using plugins such as flash ###

* This application is an html 5 player for playing hls streams ( live and vod )
* Version 1

### How do I get set up? ###

step 1 : should install nodejs




```
#!bash


sudo apt-get install nodejs
sudo apt-get install nodejs-legacy
sudo apt-get install npm

```



step 2 : clone the source code from this repository


```
#!git

git clone https://coolravanz@bitbucket.org/coolravanz/voicebox-wowza-hls-playback.git
```


step 2 : install express and socketio
```
#!bash
cd voicebox-wowza-hls-playback
sudo npm install express
sudo npm install socket.io
```
step 3 : run the application
```
#!bash
sudo node server.js
```
Now console will show the message Server Running At 8080 . Your application is ready

Please load the url  :   http://[your domain]:6060

### You can change the port on server.js file.port in the example is 6060 ###
if you have an http server you can simply copy and paste contents in static directory and use tha application without installing nodejs. nodejs is only used for serving html content

## Wowza settings for hls playback ##
* You should enable CORS to access wowza server from othe domains  for this follow the steps

### To enable CORS in your live or on demand application: ###

* In Wowza Streaming Engine Manager, click the Applications tab and then click the name of your application (such as live).
* On the application page Properties tab, click Custom in the Quick Links bar.
* Click Add Custom Property, specify the following settings in the Add Custom Property dialog box, and then click Add:

```
#!wowza


* Path - Select /Root/Application/HTTPStreamer.
* Name - Enter cupertinoUserHTTPHeaders.
* Type - Select String.
* Value - Enter Access-Control-Allow-Origin: *.
* Click Save, and then restart the application.
```


## Supported codec##
video codec :  H.264
audio codec :  aac