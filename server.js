// Load required modules
var port     =  6060;
var http    = require("http");              // http server core module
var express  = require("express");           // web framework external module
var io       = require("socket.io");         // web socket external module

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var httpApp = express();
httpApp.use(express.static(__dirname + "/static/"));


// Start Express http server on port 90
var webServer = http.createServer(httpApp).listen(port);

// Start Socket.io so it attaches itself to Express server
var socketServer = io.listen(webServer, {"log level":1});
console.log('Open url http://[yourdomain/ip]:'+port);

